package snow.zhen.zayta.main.engine.hud;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.Viewport;



public class HudSystem extends EntitySystem {
    private static final Logger log = new Logger(HudSystem.class.getName(),Logger.DEBUG);
    private final Viewport viewport;
    private final SpriteBatch batch;
    private final BitmapFont font;

    private final GlyphLayout layout = new GlyphLayout();

    private Hud hud;
    public HudSystem(int priority,Viewport viewport, SpriteBatch batch, BitmapFont font, Hud hud)
    {
        super(priority);
        this.viewport = viewport;
        this.batch = batch;
        this.hud = hud;
        this.font = font;
        font.getData().setScale(2,2);
    }

    @Override
    public void update(float deltaTime) {


        hud.getViewport().apply();
//        batch.setProjectionMatrix(hud.getCamera().combined);
        hud.draw();
        hud.act(); //draw the Hud
    }


}
