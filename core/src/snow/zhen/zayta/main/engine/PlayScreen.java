package snow.zhen.zayta.main.engine;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import snow.zhen.zayta.main.Game;
import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.assets.AssetDescriptors;
import snow.zhen.zayta.main.assets.RegionNames;
import snow.zhen.zayta.main.debug.DebugCameraSystem;
import snow.zhen.zayta.main.engine.entities.Characters;
import snow.zhen.zayta.main.engine.game_systems.character.CharacterBlockSystem;
import snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks.ColorWallSystem;
import snow.zhen.zayta.main.engine.dialogue.systems.SpeakerEmoteRenderSystem;
import snow.zhen.zayta.main.engine.dialogue.systems.DialogueRenderSystem;
import snow.zhen.zayta.main.engine.dialogue.systems.SpeakerRemovalSystem;
import snow.zhen.zayta.main.engine.game_systems.character.follower.FollowerLabelSystem;
import snow.zhen.zayta.main.engine.game_systems.character.follower.FollowerRemovalSystem;
import snow.zhen.zayta.main.engine.game_systems.holes.DragHoleSystem;
import snow.zhen.zayta.main.engine.game_systems.holes.FallInHoleSystem;
import snow.zhen.zayta.main.engine.game_systems.sokoban_doors.DoorUnlockSystem;
import snow.zhen.zayta.main.engine.hud.Hud;
import snow.zhen.zayta.main.engine.hud.HudSystem;
import snow.zhen.zayta.main.engine.input.KeyboardController;
import snow.zhen.zayta.main.engine.map.Map;
import snow.zhen.zayta.main.engine.movement.MovementSystem;
import snow.zhen.zayta.main.engine.movement.PositionsComparatorSystem;
import snow.zhen.zayta.main.engine.movement.character_behavior.FaceDirOnNoMovementSystem;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTrackerSystem;
import snow.zhen.zayta.main.engine.movement.world_wrap.WorldWrapPauseSystem;
import snow.zhen.zayta.main.engine.render.CameraUpdateSystem;
import snow.zhen.zayta.main.engine.render.animation.AnimationSystem;
import snow.zhen.zayta.main.engine.render.RenderSystem;
import snow.zhen.zayta.main.engine.render.monocolor.MonoColorEntityRenderSystem;
import snow.zhen.zayta.util.GdxUtils;
import snow.zhen.zayta.util.ViewportUtils;
//import z_oop.sokoban.input.Hud;
//import snow.zhen.zayta.main.sokoban_OOP.Puzzle;

public class PlayScreen extends ScreenAdapter {
    private Game game;
    private final AssetManager assetManager;
    // == attributes ==
    //render
    private Viewport viewport; private Viewport hudViewport, dialogueViewport;
    private OrthographicCamera camera;
    private SpriteBatch batch;

    //input
    private Hud hud;
    private PlayController playController;
    private InputMultiplexer inputMultiplexer;
    //story
//    private DialogueFileParser dialogueFileParser;
    //game elements
    private PooledEngine engine;
    private Characters characters;
    private EntityFactory entityFactory;
    private snow.zhen.zayta.main.engine.map.Map map;
    private int curLvl =0;
    private DoorUnlockSystem doorUnlockSystem;
    private FallInHoleSystem fallInHoleSystem;
    //player movement
    PositionTracker positionTracker;

    public PlayScreen(Game game){
        //gameplay data
        this.game = game;
        this.assetManager = game.getAssetManager();
        this.batch = game.getBatch();
        engine = new PooledEngine();


        camera = new OrthographicCamera();
        viewport = new FitViewport(GameConfig.VIRTUAL_WIDTH, GameConfig.VIRTUAL_HEIGHT, camera);
        dialogueViewport = new FitViewport(GameConfig.VIRTUAL_WIDTH, GameConfig.VIRTUAL_HEIGHT);
        hudViewport = new ExtendViewport(GameConfig.HUD_WIDTH,GameConfig.HUD_HEIGHT);
        //game elements
        TextureAtlas textureAtlas = assetManager.get(AssetDescriptors.GAMEPLAY);
        this.characters = new Characters(textureAtlas);
        this.entityFactory = new EntityFactory(engine,textureAtlas,characters);
        this.map = new Map(entityFactory,characters);
        positionTracker = new PositionTracker(
                Math.max(Math.max(map.getMapWidth(),map.getMapHeight()),
                        Math.max(GameConfig.VIRTUAL_WIDTH,GameConfig.VIRTUAL_HEIGHT)));


        this.playController = new PlayController(engine,this,positionTracker,characters);
//        this.dialogueFileParser = new DialogueFileParser();
        setInput();

    }
    private void setInput(){
        inputMultiplexer = new InputMultiplexer();


        inputMultiplexer.addProcessor(new KeyboardController(playController));
        this.hud = new Hud(playController,game,hudViewport);
        inputMultiplexer.addProcessor(hud);
//        inputMultiplexer.addProcessor(new GestureDetector(new SwipeController(controller)));

        Gdx.input.setInputProcessor(inputMultiplexer);
    }




    @Override
    public void show() {
        init();
    }
    public void init(){
//        hud.setShowSettings(false);
//        engine.update(0);
        engine.removeAllEntities();//need this call!
        Gdx.input.setInputProcessor(inputMultiplexer);

        addEntities();//entitiies sb added b4 systems

        addSystems();
    }
    private void addEntities(){
        map.init(curLvl);
        positionTracker.init(Math.max(Math.max(map.getMapWidth(),map.getMapHeight()),
                Math.max(GameConfig.VIRTUAL_WIDTH, GameConfig.VIRTUAL_HEIGHT)));

    }

    private void addSystems(){
//        engine.addSystem(new InputSystem(20));
        engine.addSystem(new PositionsComparatorSystem(1));
        engine.addSystem(new PositionTrackerSystem(10,positionTracker));//updates the tracker
        engine.addSystem(new WorldWrapPauseSystem(30));

        engine.addSystem(new ColorWallSystem(30,positionTracker)); //finalize target positions of character
        engine.addSystem(new CharacterBlockSystem(40,positionTracker));

        engine.addSystem(new FollowerRemovalSystem(60));
        engine.addSystem(new SpeakerRemovalSystem(60));

        engine.addSystem(new DragHoleSystem(50));
//        engine.addSystem(new FollowerSystem());

        this.fallInHoleSystem = new FallInHoleSystem(60,positionTracker);
        engine.addSystem(fallInHoleSystem);

        engine.addSystem(new MovementSystem(70));//moves entity to target position n set movement to none. should be last
        engine.addSystem(new CameraUpdateSystem(10,viewport,map.getMapWidth(),map.getMapHeight()));



        //add game state systems
        DoorUnlockSystem doorUnlockSystem = new DoorUnlockSystem(90,positionTracker);
        this.doorUnlockSystem = doorUnlockSystem;
        engine.addSystem(doorUnlockSystem);



        engine.addSystem(new RenderSystem(120,viewport,batch));
        engine.addSystem(new MonoColorEntityRenderSystem(130,viewport));

        engine.addSystem(new FollowerLabelSystem(140,viewport,batch));

//        engine.addSystem(new DialogueFactory(15,engine,curLvl));
        //put all render systems before hud system.
//        engine.addSystem(new SpeakerRenderSystem(16,viewport,batch));
        engine.addSystem(new DialogueRenderSystem(150,dialogueViewport,batch,
                assetManager.get(AssetDescriptors.GAMEPLAY).findRegion(RegionNames.DIALOGUE_BOX)));

        engine.addSystem(new SpeakerEmoteRenderSystem(140,viewport,batch,
                assetManager.get(AssetDescriptors.GAMEPLAY).findRegion(RegionNames.EMOTE_DOTS3)));

        engine.addSystem(new HudSystem(980,viewport,batch,assetManager.get(AssetDescriptors.FONT),hud));

        addDebugSystems();
        addAnimationSystems();


    }

    private void addAnimationSystems(){
        engine.addSystem(new AnimationSystem(15));
        engine.addSystem(new FaceDirOnNoMovementSystem(16));
    }














    private void addDebugSystems(){

        engine.addSystem(new DebugCameraSystem(99,camera, GameConfig.VIRTUAL_CENTER_X, GameConfig.VIRTUAL_CENTER_Y));
//        engine.addSystem(new DebugPositionTrackerSystem(100,positionTracker,viewport,batch));
    }
    @Override
    public void render(float delta) {
        GdxUtils.clearScreen();
//        controller.update(delta);

//        hud.act(delta); //act the Hud
        engine.update(delta);
//        if(controller.isComplete())
//            progress();

        if(fallInHoleSystem.isLvlFailed()){
            //System.out.println("Lvl failed");
            fail();
        }
        if(doorUnlockSystem.wallsPositioned()) {

            //System.out.println("Lvl passed");
            progress();
        }
    }

    @Override
    public void resize(int width, int height) {
        GameConfig.configScreenOrientation(width,height);
        viewport.setWorldSize(Math.min(GameConfig.VIRTUAL_WIDTH,map.getMapWidth()),Math.min(GameConfig.VIRTUAL_HEIGHT, map.getMapHeight()));

//        viewport.setWorldSize(map.getMapWidth(),map.getMapHeight());
//        viewport.setWorldSize(Math.max(GameConfig.VIRTUAL_WIDTH,map.getMapWidth()),Math.max(GameConfig.VIRTUAL_HEIGHT, map.getMapHeight()));
//        viewport.setWorldSize(GameConfig.VIRTUAL_WIDTH,GameConfig.VIRTUAL_HEIGHT);
        viewport.update(width, height,true);

        ////System.out.println("Positions: "+positionTracker.toString());

        hudViewport.setWorldSize(GameConfig.HUD_WIDTH,GameConfig.HUD_HEIGHT);
        hudViewport.update(width, height,true);
        hud.resize(width,height);

        dialogueViewport.setWorldSize(GameConfig.VIRTUAL_WIDTH,GameConfig.VIRTUAL_HEIGHT);
        dialogueViewport.update(width,height,true);


        ViewportUtils.debugPixelsPerUnit(viewport);
        ViewportUtils.debugPixelsPerUnit(hud.getViewport());
    }

    @Override
    public void hide() {
//        dispose();
//        controller.hide();
    }

    @Override
    public void dispose() {
        hud.dispose();
//        renderer.dispose();
        batch.dispose();
    }


    /****For screen transition*****/
    public void progress(){
        reset();
        game.complete(curLvl);

    }
    public void fail(){
        //System.out.println("failed");
        restart();
//        game.setMainScreen();
    }
    public void restart(){
        reset();
        init();
    }

    /*For level management*/
    public void setLvl(int lvl){

        this.curLvl = Math.min(lvl,map.getTotalLvls());
    }

    private void reset(){
        positionTracker.reset();
        engine.removeAllEntities();
//        engine.clearPools();
//        for(EntitySystem system:engine.getSystems()){
//            engine.removeSystem(system);
//        }
//        engine.update(0);

    }

}
