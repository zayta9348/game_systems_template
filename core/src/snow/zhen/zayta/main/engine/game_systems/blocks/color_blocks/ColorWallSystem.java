package snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.tags.CharacterTag;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;

import snow.zhen.zayta.main.engine.entities.EntityType;

public class ColorWallSystem extends IteratingSystem {
//    private PooledEngine engine;
    private PositionTracker positionTracker;
    private static final Family family = Family.all(
            MovementComponent.class,
            CharacterTag.class,
            ColorComponent.class).get();
    public ColorWallSystem(int priority,PositionTracker positionTracker) {
        super(family,priority);
//        this.engine = engine;
        this.positionTracker = positionTracker;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        MovementComponent movementComponent = Mappers.MOVEMENT.get(entity);
        Vector2 targetPosition = movementComponent.getTargetPosition();
        Entity collidedWall = positionTracker.getEntityAtPos(EntityType.WALL,targetPosition.x,targetPosition.y);
//        //System.out.println("Collided wall is "+collidedWall);
        if(collidedWall!=null){


            ////System.out.println("Target position is "+targetPosition+" and wall at that position is "+collidedWall);
            Vector2 newPos = new Vector2(targetPosition.x+movementComponent.getDirection().directionX,
                    targetPosition.y+movementComponent.getDirection().directionY);

            if(canMoveWall(entity,collidedWall,newPos)){
                MovementComponent wallMovement = Mappers.MOVEMENT.get(collidedWall);
                if(wallMovement!=null) {
                    wallMovement.setDirection(movementComponent.getDirection());
                    wallMovement.setTargetPosition(newPos);
                }
            }
            else{

                targetPosition.set(Mappers.POSITION.get(entity).getPosition()); //stop entity from moving
//                targetPosition.sub(movementComponent.getDirection().vector);
            }

        }


    }

    private boolean canMoveWall(Entity entity,Entity collidedWall,Vector2 collidedWallNewPos){
        Entity collidedEntity = positionTracker.getEntityAtPos(EntityType.WALL,collidedWallNewPos.x,collidedWallNewPos.y);
//        return collidedEntity==null;
        if(collidedEntity==null){ //if theres no entity behind the wall
            //match the wall's color to the entities color
            Color wallColor = Mappers.COLOR.get(collidedWall).getColor();

//            //System.out.println("Wallcolor is "+wallColor);
            if(wallColor!=null) {
                Color entityColor = Mappers.COLOR.get(entity).getColor();

                return wallColor.equals(entityColor);
            }
            else
                return true;

        }
        else{
//            Color wallColor = Mappers.COLOR.get(collidedEntity).getColor();
//
//            //System.out.println("Wallcolor is "+wallColor);
//            if(wallColor!=null) {
//                Color entityColor = Mappers.COLOR.get(entity).getColor();
//
//                return wallColor.equals(entityColor);
//            }
//            else return true;
            return false;

        }
    }




}
