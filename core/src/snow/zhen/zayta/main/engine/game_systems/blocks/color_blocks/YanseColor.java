package snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks;

import com.badlogic.gdx.graphics.Color;

public enum YanseColor {
    RED(Color.FIREBRICK),
    ORANGE(new Color(244/255f, 224/255f, 181/255f,1)),
    GREEN(new Color(0xf2bc65)),
    BLUE(new Color(25/255f,25/255f,112/255f,1)),
    LAVENDER(new Color(0xE6E6FAFF)),

    GRAY(new Color(.5f,.5f,.5f,.8f)),
    WHITE(Color.WHITE);

    public final Color color;
    YanseColor(Color color){
        this.color = color;
    }

    //    public static final Color RED = Color.FIREBRICK;
//    public static final Color ORANGE = new Color(244/255f, 224/255f, 181/255f,1);
//    public static final Color GREEN = new Color(0xf2bc65);
//    public static final Color BLUE = new Color(25/255f,25/255f,112/255f,1);
//    public static final Color LAVENDER = new Color(0xE6E6FAFF);
//
//
//    public static final Color GRAY = new Color(.5f,.5f,.5f,.8f);
//    public static final Color WHITE = Color.WHITE;
////    public static final Color BLACK = Color.BLACK;

    public static final Color get(char id){
        Color color = YanseColor.GRAY.color;
        switch (Character.toUpperCase(id)){
            case 'W':
                color = YanseColor.WHITE.color;
            break;
            case 'R':
                color = YanseColor.RED.color;
                break;
            case 'O':
                color = YanseColor.ORANGE.color;
                break;
            case 'B':
                color = YanseColor.BLUE.color;
                break;
            case 'G':
                color = YanseColor.GREEN.color;
                break;
            case 'L':
                color = YanseColor.LAVENDER.color;
                break;
        }
        return color;
    }

}
