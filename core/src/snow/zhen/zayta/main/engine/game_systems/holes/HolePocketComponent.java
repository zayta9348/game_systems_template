package snow.zhen.zayta.main.engine.game_systems.holes;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.Pool;

import java.util.ArrayList;


public class HolePocketComponent implements Component, Pool.Poolable {

    private ArrayList<Entity> holes;

    public HolePocketComponent(){
        this.holes = new ArrayList<Entity>();
    }
    public void addHole(Entity hole){
        this.holes.add(hole);
    }



    public ArrayList<Entity> getHoles() {
        return holes;
    }


    @Override
    public void reset() {
        holes.clear();
    }
}
