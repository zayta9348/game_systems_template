package snow.zhen.zayta.main.engine.game_systems.sokoban_doors;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.properties.GoalComponent;
import snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks.ColorComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;

import snow.zhen.zayta.main.engine.entities.EntityType;

public class DoorUnlockSystem extends EntitySystem {
    private PositionTracker positionTracker; private int numGoals;
    private boolean wallsPositioned;
    private static final Family goals = Family.all(GoalComponent.class).get();
    private static final Family doorsFamily = Family.all(DoorComponent.class, ColorComponent.class).get();

    
    public DoorUnlockSystem(int priority, PositionTracker positionTracker) {
        super(priority);
        this.positionTracker = positionTracker;
        this.wallsPositioned = false;
    }
    @Override
    public void update(float deltaTime) {
        ImmutableArray<Entity> entities = getEngine().getEntitiesFor(goals);
        this.numGoals = entities.size();
        for(Entity entity:entities) {
            processEntity(entity,deltaTime);

        }
    }
    //todo figure out way to count by color how many goals were placed.
    // Sometimes: Only one door may open. when one door open, another close.
    // Some goals have color so only walls of that color plced on tht goal would count. other goals don't have color.

    int inPos = 0;
    protected void processEntity(Entity entity, float deltaTime) {
//        ////System.out.println("Goal "+entity+" processed");
        Position position = Mappers.POSITION.get(entity);
        inPos = 0;
        if(positionTracker.getEntityAtPos(EntityType.WALL,position.getX(),position.getY())!=null){
            inPos++;
        };
        if(inPos>=numGoals){

            System.out.println("IsComplete: "+wallsPositioned+"inPos is "+inPos+" numGoals is "+numGoals);
            wallsPositioned = true;
        }
    }
    
    public void unlockDoor(Color color){
        ImmutableArray<Entity> doors = getEngine().getEntitiesFor(doorsFamily);
        for(Entity door:doors){
            if(Mappers.COLOR.get(door).getColor().equals(color)){
                Mappers.DOOR.get(door).setUnlocked(true);
            }
        }

    }

    public boolean wallsPositioned() {
//        System.out.println("IsComplete: "+wallsPositioned+"inPos is "+inPos+" numGoals is "+numGoals);
        return wallsPositioned;
    }
}
