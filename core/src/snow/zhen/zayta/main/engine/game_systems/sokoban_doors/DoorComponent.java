package snow.zhen.zayta.main.engine.game_systems.sokoban_doors;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class DoorComponent implements Component, Pool.Poolable {
    private boolean unlocked = false;

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    @Override
    public void reset() {
        unlocked = false;
    }
}
