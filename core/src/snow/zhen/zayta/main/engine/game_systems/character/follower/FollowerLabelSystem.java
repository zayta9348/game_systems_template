package snow.zhen.zayta.main.engine.game_systems.character.follower;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.Viewport;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.game_systems.hand_hold.FollowerTag;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTrackerComponent;

public class FollowerLabelSystem extends IteratingSystem {
    private static final Logger log = new Logger(FollowerLabelSystem.class.getName(), Logger.DEBUG);
    private final Viewport viewport;
    private final SpriteBatch batch;
    private final BitmapFont font;

    private final GlyphLayout layout = new GlyphLayout();

    private Array<Entity> renderQueue = new Array<Entity>();
    private static final Family FOLLOWERS = Family.all(FollowerTag.class, MovementComponent.class).get();



    public FollowerLabelSystem(int priority,Viewport viewport, SpriteBatch batch) {
        super(FOLLOWERS,priority);
        this.viewport = viewport;
        this.batch = batch;
        /**Customize Font**/
        this.font = new BitmapFont();
        float scaleX = GameConfig.VIRTUAL_WIDTH / GameConfig.WIDTH;
        float scaleY = GameConfig.VIRTUAL_HEIGHT / GameConfig.HEIGHT;
        float fontScale = 2;
        font.setUseIntegerPositions(false);
        font.setColor(Color.CYAN);
        font.getData().setScale(fontScale * scaleX, fontScale * scaleY);


    }

    @Override
    public void update(float deltaTime) {

        ImmutableArray<Entity> entities = getEngine().getEntitiesFor(FOLLOWERS);
        renderQueue.addAll(entities.toArray());

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();

        super.update(deltaTime);

        batch.end();

        renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Position position = Mappers.POSITION.get(entity);
        if(Mappers.COLOR.get(entity)!=null){
            font.setColor(Mappers.COLOR.get(entity).getColor());
        }


        layout.setText(font,/*"Position: ("+position.getX()+","+position.getY()+")\n"+
                "Position Raw Key: "+PositionTracker.generateKey(position.getX(),position.getY())+"\n"+
                "Bounds Raw Key: "+PositionTracker.generateKey(bounds.getX(),bounds.getY())+"\n"+*/
                /*"\\../"*/"o/\\o");
        font.draw(batch, layout, position.getX()+GameConfig.ENTITY_SIZE/2+0.25f, position.getY() + GameConfig.ENTITY_SIZE + layout.height+0.1f);//0.1f is offset from bottom
    }


}