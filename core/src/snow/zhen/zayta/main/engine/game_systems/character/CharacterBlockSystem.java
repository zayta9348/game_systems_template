package snow.zhen.zayta.main.engine.game_systems.character;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.tags.CharacterTag;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;

import snow.zhen.zayta.main.engine.entities.EntityType;

import static snow.zhen.zayta.main.engine.movement.PositionsComparator.positionsComparator;

public class CharacterBlockSystem extends SortedIteratingSystem {
    //    private PooledEngine engine;
    private PositionTracker positionTracker;
    private static final Family family = Family.all(
            MovementComponent.class,
            CharacterTag.class).get();
    public CharacterBlockSystem(int priority,PositionTracker positionTracker) {
        super(family,positionsComparator,priority);
//        this.engine = engine;
        this.positionTracker = positionTracker;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        forceSort();
        MovementComponent movementComponent = Mappers.MOVEMENT.get(entity);
        Vector2 targetPosition = movementComponent.getTargetPosition();
        Entity collidedCharacter = positionTracker.getEntityAtPos(EntityType.CHARACTER,targetPosition.x,targetPosition.y);
//        if(Mappers.PLAYER.get(entity)!=null)
//        System.out.println("Target position decide btwn "+Mappers.POSITION.get(entity)+" to "+targetPosition+", and equals is "+Mappers.POSITION.get(entity).getPosition().equals(targetPosition));

        if(collidedCharacter!=null&&collidedCharacter!=entity){
//            System.out.println("Collided entity is "+collidedCharacter);
//
//            ////System.out.println("Target position is "+targetPosition+" and wall at that position is "+collidedCharacter);
//            Vector2 newPos = new Vector2(targetPosition.x+movementComponent.getDirection().directionX,
//                    targetPosition.y+movementComponent.getDirection().directionY);
//
//            if(canMoveCharacter(entity,collidedCharacter,newPos)){
//                MovementComponent wallMovement = Mappers.MOVEMENT.get(collidedCharacter);
//                if(wallMovement!=null) {
//                    wallMovement.setDirection(movementComponent.getDirection());
//                    wallMovement.setTargetPosition(newPos);
//                }
//            }
//            else{
                movementComponent.setTargetPosition(Mappers.POSITION.get(entity).getPosition());
//                targetPosition.set(Mappers.POSITION.get(entity).getPosition()); //stop entity from moving
//            }
//                System.out.println("Target position has been set from "+Mappers.POSITION.get(entity)+" to "+targetPosition);

        }


    }

    private boolean canMoveCharacter(Entity entity,Entity collidedCharacter,Vector2 collidedCharacterNewPos){
        Entity collidedEntity = positionTracker.getEntityAtPos(EntityType.WALL,collidedCharacterNewPos.x,collidedCharacterNewPos.y);
//        return collidedEntity==null;
        if(collidedEntity==null){ //if theres no entity behind the wall
            //match the wall's color to the entities color
            Color wallColor = Mappers.COLOR.get(collidedCharacter).getColor();

            //System.out.println("Charactercolor is "+wallColor);
            if(wallColor!=null) {
                Color entityColor = Mappers.COLOR.get(entity).getColor();

                return wallColor.equals(entityColor);
            }
            else
                return true;

        }
        else{
//            Color wallColor = Mappers.COLOR.get(collidedEntity).getColor();
//
//            //System.out.println("Charactercolor is "+wallColor);
//            if(wallColor!=null) {
//                Color entityColor = Mappers.COLOR.get(entity).getColor();
//
//                return wallColor.equals(entityColor);
//            }
//            else return true;
            return false;

        }
    }




}

