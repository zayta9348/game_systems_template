package snow.zhen.zayta.main.engine.game_systems.holes;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import java.awt.Point;
import java.util.ArrayList;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;

public class FallInHoleSystem extends IteratingSystem {
    private static final Family holes = Family.all(HoleTag.class, Position.class).get();
    private PositionTracker positionTracker;

    private boolean lvlFailed;
    public FallInHoleSystem(int priority,PositionTracker positionTracker) {
        super(holes,priority);
        this.positionTracker = positionTracker;
        lvlFailed = false;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Position position = Mappers.POSITION.get(entity);
        ArrayList<Entity> entitiesToBeRemoved = positionTracker.getSolidEntitiesAtPos(position.getX(),position.getY());
        for(Entity e: entitiesToBeRemoved){
            if(entity!=e) {
                //System.out.println("Entity "+e +" has been removed");
                getEngine().removeEntity(e);
                positionTracker.removeEntity(e);
                if(Mappers.PLAYER.get(e)!=null){ //if it is the player that fell into the hole
                   lvlFailed = true;
                }
            }
        }

    }

    public boolean isLvlFailed() {
        return lvlFailed;
    }

}
