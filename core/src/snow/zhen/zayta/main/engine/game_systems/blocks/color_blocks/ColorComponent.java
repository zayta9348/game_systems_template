package snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Pool;

public class ColorComponent implements Component, Pool.Poolable {


    private Color color = Color.WHITE;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void reset() {
        color = Color.WHITE;
    }
}
