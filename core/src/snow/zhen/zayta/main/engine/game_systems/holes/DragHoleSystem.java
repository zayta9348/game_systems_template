package snow.zhen.zayta.main.engine.game_systems.holes;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import java.util.ArrayList;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.movement.Direction;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;

public class DragHoleSystem extends IteratingSystem {

    private static final Family family = Family.all(HolePocketComponent.class, MovementComponent.class).get();
    public DragHoleSystem(int priority) {
        super(family,priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        MovementComponent movementComponent = Mappers.MOVEMENT.get(entity);
//        ////System.out.println("Hole "+entity+" is moved");
        HolePocketComponent holePocketComponent = Mappers.HOLE_POCKET.get(entity);
        ArrayList<Entity> holes = holePocketComponent.getHoles();
        ////System.out.println("Size of holes is "+holes.size());
        moveHoles(holes,movementComponent.getDirection(),
                Math.round(movementComponent.getTargetPosition().x),
                Math.round(movementComponent.getTargetPosition().y));

    }

    public void moveHoles(ArrayList<Entity> holes,Direction direction, float x, float y){
//        Direction direction = Direction.getOpposite(d);
        switch (direction){
            case up:
                moveHolesUp(holes,x,y);
                break;
            case down:
                moveHolesDown(holes,x,y);
                break;
            case left:
                moveHolesLeft(holes,x,y);
                break;
            case right:
                moveHolesRight(holes,x,y);
                break;
        }
    }
    private void moveHolesDown(ArrayList<Entity> holes,float x, float y){
        for(int i = 0 ; i<holes.size(); i++){
            MovementComponent movementComponent = Mappers.MOVEMENT.get(holes.get(i));
            if(movementComponent!=null)
            movementComponent.setTargetPosition(x,y+1+i);
        }
    }

    private void moveHolesLeft(ArrayList<Entity> holes,float x, float y){
        for(int i = 0 ; i<holes.size(); i++){
            MovementComponent movementComponent = Mappers.MOVEMENT.get(holes.get(i));
            if(movementComponent!=null)
            movementComponent.setTargetPosition(x+1+i,y);
        }
    }

    private void moveHolesRight(ArrayList<Entity> holes,float x, float y){
        for(int i = 0 ; i<holes.size(); i++){
            MovementComponent movementComponent = Mappers.MOVEMENT.get(holes.get(i));
            if(movementComponent!=null)
            movementComponent.setTargetPosition(x-1-i,y);
        }
    }
    private void moveHolesUp(ArrayList<Entity> holes,float x, float y){
        for(int i = 0 ; i<holes.size(); i++){
            MovementComponent movementComponent = Mappers.MOVEMENT.get(holes.get(i));
            if(movementComponent!=null)
            movementComponent.setTargetPosition(x,y-1-i);
        }
    }
}
