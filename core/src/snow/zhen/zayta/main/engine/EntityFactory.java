package snow.zhen.zayta.main.engine;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.assets.RegionNames;
import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.Characters;
import snow.zhen.zayta.main.engine.entities.properties.GoalComponent;
import snow.zhen.zayta.main.engine.entities.properties.WallComponent;
import snow.zhen.zayta.main.engine.entities.tags.CharacterTag;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks.ColorComponent;
import snow.zhen.zayta.main.engine.dialogue.DialogueFactory;
import snow.zhen.zayta.main.engine.game_systems.holes.HolePocketComponent;
import snow.zhen.zayta.main.engine.game_systems.holes.HoleTag;
import snow.zhen.zayta.main.engine.movement.Direction;
import snow.zhen.zayta.main.engine.movement.character_behavior.FaceDirOnNoMovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTrackerComponent;
import snow.zhen.zayta.main.engine.movement.world_wrap.WorldWrapComponent;
import snow.zhen.zayta.main.engine.render.animation.AnimationComponent;
import snow.zhen.zayta.main.engine.render.TextureComponent;
import snow.zhen.zayta.main.engine.render.monocolor.MonoColorRenderTag;

import snow.zhen.zayta.main.engine.entities.EntityType;
import snow.zhen.zayta.util.BiMap;

public class EntityFactory {
    private PooledEngine engine; private TextureAtlas textureAtlas;
    private DialogueFactory dialogueFactory;

    //stores the charactername and the corresponding entity in this lvl
    private static BiMap<Characters.CharacterName,Entity> charactersEntityBimap = new BiMap<Characters.CharacterName, Entity>();;

    private Characters characters; private float mapWidth,mapHeight;
    public EntityFactory(PooledEngine engine, TextureAtlas textureAtlas, Characters characters){
        this.engine = engine;
        this.textureAtlas = textureAtlas;

        this.characters = characters;
        dialogueFactory = new DialogueFactory(engine,characters);
//        charactersEntityBimap = new BiMap<CharacterName, Entity>();
//        initCharacters();
    }
    public void init(float mapWidth, float mapHeight){
        charactersEntityBimap.clear();
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
    }

    public void addPlayer(Characters.CharacterName characterName, float x, float y){
        Entity player = engine.createEntity();
        PlayerTag playerTag = engine.createComponent(PlayerTag.class);
        player.add(playerTag);

        CharacterTag characterTag = engine.createComponent(CharacterTag.class);
        characterTag.setCharacterName(characterName);
        player.add(characterTag);

        addMovementComponents(player, EntityType.CHARACTER,x,y);

        if(characterName== Characters.CharacterName.TENYU)
            player.add(holePocket(1, Mappers.POSITION.get(player),Direction.getOpposite(Mappers.MOVEMENT.get(player).getDirection())));


        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);

        textureComponent.init(characters.getTexture(characterName).get(0), GameConfig.CHARACTER_RENDER_OFFSET,GameConfig.CHARACTER_RENDER_WIDTH,GameConfig.ENTITY_SIZE);
        player.add(textureComponent);

        ColorComponent colorComponent = engine.createComponent(ColorComponent.class);
        if(characters.getColor(characterName)!=null)
            colorComponent.setColor(characters.getColor(characterName));
        player.add(colorComponent);
        
        AnimationComponent animationComponent = engine.createComponent(AnimationComponent.class);
        animationComponent.init(characters.getTexture(characterName));
        player.add(animationComponent);

        ////System.out.println("Player is at position "+Mappers.POSITION.get(player).getPosition().toString());
        engine.addEntity(player);
//        return player;
        charactersEntityBimap.put(characterName,player);
    }

    public void addNighter(Characters.CharacterName characterName, float x, float y, int lvl){
        Entity nighter = engine.createEntity();
//        nighter.add(engine.createComponent(FollowerTag.class));
        CharacterTag characterTag = engine.createComponent(CharacterTag.class);
        characterTag.setCharacterName(characterName);
        nighter.add(characterTag);

        addMovementComponents(nighter,EntityType.CHARACTER,x,y);
        Mappers.MOVEMENT.get(nighter).setDirection(Direction.down);

        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
        textureComponent.init(characters.getTexture(characterName).get(0), GameConfig.CHARACTER_RENDER_OFFSET,GameConfig.CHARACTER_RENDER_WIDTH,GameConfig.ENTITY_SIZE);
        nighter.add(textureComponent);


        ColorComponent colorComponent = engine.createComponent(ColorComponent.class);
        if(characters.getColor(characterName)!=null)
        colorComponent.setColor(characters.getColor(characterName));
        nighter.add(colorComponent);
        
        AnimationComponent animationComponent = engine.createComponent(AnimationComponent.class);
        animationComponent.init(characters.getTexture(characterName));
        nighter.add(animationComponent);

        nighter.add(engine.createComponent(FaceDirOnNoMovementComponent.class));



        dialogueFactory.addDialogue(nighter,characterName,lvl);

        engine.addEntity(nighter);
//        return nighter;

        charactersEntityBimap.put(characterName,nighter);
    }

    public void addWall(float x, float y, Color color){
        Entity wall = engine.createEntity();
        wall.add(engine.createComponent(WallComponent.class));

        addMovementComponents(wall,EntityType.WALL,x,y);

        ColorComponent colorComponent = engine.createComponent(ColorComponent.class);
        colorComponent.setColor(color);
        wall.add(colorComponent);
        
        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
        textureComponent.init(textureAtlas.findRegion(RegionNames.BRICK));
        wall.add(textureComponent);
        wall.add(engine.createComponent(MonoColorRenderTag.class));
        engine.addEntity(wall);
//        return wall;
    }
    public void addGoal(float x, float y,Color color){
        Entity goal = engine.createEntity();
        GoalComponent goalComponent = engine.createComponent(GoalComponent.class);
        goal.add(goalComponent);
        addMovementComponents(goal,EntityType.GOAL,x,y);
        ColorComponent colorComponent = engine.createComponent(ColorComponent.class);
        colorComponent.setColor(color);
        goal.add(colorComponent);
        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
        textureComponent.init(textureAtlas.findRegion(RegionNames.OVERLAY[3]));
        goal.add(textureComponent);

        engine.addEntity(goal);
//        return goal;
    }

    private void addMovementComponents(Entity entity,EntityType entityType, float x, float y){

        Position position = engine.createComponent(Position.class);
        position.init(x,y);
        entity.add(position);

        PositionTrackerComponent positionTrackerComponent = engine.createComponent(PositionTrackerComponent.class);
        positionTrackerComponent.init(entityType);
        entity.add(positionTrackerComponent);

        MovementComponent movementComponent = engine.createComponent(MovementComponent.class);
        movementComponent.init(new Vector2(position.getX(),position.getY()));
        movementComponent.setDirection(Direction.none);
        entity.add(movementComponent);

        WorldWrapComponent worldWrapComponent = engine.createComponent(WorldWrapComponent.class);
        worldWrapComponent.setBoundsOfMovement(0,0,mapWidth,mapHeight);
        entity.add(worldWrapComponent);

    }
    private HolePocketComponent holePocket(int numHoles,Position position, Direction direction){
        direction = Direction.down;
        HolePocketComponent holePocketComponent= engine.createComponent(HolePocketComponent.class);
        for(int i = 1; i<=numHoles; i++){

            Entity hole = engine.createEntity();
            HoleTag holeTag = engine.createComponent(HoleTag.class);
            hole.add(holeTag);

            addMovementComponents(hole,EntityType.HOLE,position.getX()+i*GameConfig.ENTITY_SIZE*direction.directionX,
                    position.getY()+i*GameConfig.ENTITY_SIZE*direction.directionY);
            ////System.out.println("Hole is at position "+Mappers.POSITION.get(hole).getPosition().toString());
            //add texture componnet
            TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
            textureComponent.init(textureAtlas.findRegion(RegionNames.HOLE));
            hole.add(textureComponent);

            holePocketComponent.addHole(hole);
            engine.addEntity(hole);
        }
        return holePocketComponent;
    }

    public void addBackground(float mapWidth,float mapHeight, float x, float y){
        Entity background = engine.createEntity();
        Position position = engine.createComponent(Position.class);
        position.init(x,y);
        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
        textureComponent.init(textureAtlas.findRegions(RegionNames.FLOOR_PLAIN).get(0),0,mapWidth,mapHeight);

        background.add(position);
        background.add(textureComponent);

        engine.addEntity(background);
    }
    public void addFloorTile(float width,float height, float x, float y){
        Entity background = engine.createEntity();
        Position position = engine.createComponent(Position.class);
        position.init(x,y);
        TextureComponent textureComponent = engine.createComponent(TextureComponent.class);
        textureComponent.init(textureAtlas.findRegions(RegionNames.GROUND_GRAY).get(0),0,width,height);

        background.add(position);
        background.add(textureComponent);

        engine.addEntity(background);
    }
    
    

    public static Entity getCharacter(Characters.CharacterName characterName){
        Entity character = charactersEntityBimap.get(characterName);
        if(character==null){
            System.out.println("ENtityfactory: No character with that charactername in this map.");
        }
        return character;

    }



}
