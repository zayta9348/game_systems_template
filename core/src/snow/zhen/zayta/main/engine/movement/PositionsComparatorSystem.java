package snow.zhen.zayta.main.engine.movement;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.game_systems.hand_hold.FollowerTag;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;

import static snow.zhen.zayta.main.engine.movement.PositionsComparator.positionsComparator;

public class PositionsComparatorSystem extends IteratingSystem {

    private final static Family PLAYABLE_CHARACTERS = Family.all(
            MovementComponent.class

    ).one(PlayerTag.class, FollowerTag.class).get();
    public PositionsComparatorSystem(int priority) {
        super(PLAYABLE_CHARACTERS, priority);
    }


    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if(Mappers.MOVEMENT.get(entity).getDirection()!=Direction.none){

            positionsComparator.setDirection(Mappers.MOVEMENT.get(entity).getDirection());
        }

    }
}
