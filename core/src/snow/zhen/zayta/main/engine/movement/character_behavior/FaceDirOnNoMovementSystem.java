package snow.zhen.zayta.main.engine.movement.character_behavior;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.game_systems.hand_hold.FollowerTag;
import snow.zhen.zayta.main.engine.movement.Direction;
import snow.zhen.zayta.main.engine.render.animation.AnimationComponent;

public class FaceDirOnNoMovementSystem extends IteratingSystem {
    private static final Family family = Family.all(
            FaceDirOnNoMovementComponent.class,
            AnimationComponent.class).exclude(PlayerTag.class,FollowerTag.class).get();
    public FaceDirOnNoMovementSystem(int priority) {
        super(family,priority);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Mappers.ANIMATION.get(entity).setCurrentAnimation(Mappers.DEFAULT_DIRECTION.get(entity).getDirection());
    }
}
