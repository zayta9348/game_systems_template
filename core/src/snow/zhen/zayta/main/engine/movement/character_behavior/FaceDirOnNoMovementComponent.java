package snow.zhen.zayta.main.engine.movement.character_behavior;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

import snow.zhen.zayta.main.engine.movement.Direction;

public class FaceDirOnNoMovementComponent implements Component, Pool.Poolable {
    private Direction direction = Direction.down;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public void reset() {
        direction = Direction.down;
    }
}
