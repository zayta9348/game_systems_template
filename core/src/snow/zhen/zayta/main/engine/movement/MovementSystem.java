package snow.zhen.zayta.main.engine.movement;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.math.Vector2;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;

import static snow.zhen.zayta.main.engine.movement.PositionsComparator.positionsComparator;

public class MovementSystem extends SortedIteratingSystem {
    private final static Family FAMILY = Family.all(
            MovementComponent.class,
            Position.class
    ).get();
    public MovementSystem(int priority) {
        super(FAMILY,positionsComparator,priority);
    }

    @Override
    protected void processEntity(Entity entity, float delta) {
        //todo check for change then force sort
        forceSort();
        MovementComponent movementComponent = Mappers.MOVEMENT.get(entity);

        Vector2 position = Mappers.POSITION.get(entity).getPosition();


        if (!position.equals(movementComponent.getTargetPosition())) {

            Vector2 targetPosition = movementComponent.getTargetPosition();
            ////System.out.println("Moving "+entity+" from "+position+" to "+movementComponent.getTargetPosition());
            float progress = delta * GameConfig.MOVING_SPEED;
            position.lerp(targetPosition, progress);

//            position.set(targetPosition);
            //if character pos and target pos are almost the same
            if (position.dst(targetPosition) < 0.01f) {
                movementComponent.setDirection(Direction.none);
                position.set(targetPosition);
            }
        }
        else{
            movementComponent.setDirection(Direction.none);
        }

    }
}
