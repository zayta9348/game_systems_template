package snow.zhen.zayta.main.engine;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.entities.Characters;
import snow.zhen.zayta.main.engine.entities.EntityType;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent;
import snow.zhen.zayta.main.engine.dialogue.components.SpeakerTag;
import snow.zhen.zayta.main.engine.dialogue.components.SpeechTriggererTag;
import snow.zhen.zayta.main.engine.game_systems.hand_hold.FollowerTag;
import snow.zhen.zayta.main.engine.movement.Direction;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTracker;

import static snow.zhen.zayta.main.engine.movement.PositionsComparator.positionsComparator;
//

//import z_oop.sokoban.entity.entities.Player;

public class PlayController {

    private PooledEngine engine; private ImmutableArray<Entity> entities;
    private PlayScreen playScreen;
    private PositionTracker positionTracker;
    private Characters characters;


    private final static Family PLAYABLE_CHARACTERS = Family.all(
            MovementComponent.class

    ).one(PlayerTag.class, FollowerTag.class).get();

    public PlayController(PooledEngine engine, PlayScreen playScreen, PositionTracker positionTracker, Characters characters){
        this.engine = engine;
        this.positionTracker = positionTracker;
        this.playScreen = playScreen;
        this.characters = characters;
    }

    public void movePlayer(Direction direction){

        entities = engine.getEntitiesFor(PLAYABLE_CHARACTERS);

        Array<Entity> entityArray = new Array<Entity>(entities.toArray());
//        for(Entity entity:entityArray){
////            if(Mappers.MOVEMENT.get(entity).getDirection()!=Direction.none){
//
//                positionsComparator.setDirection(Mappers.MOVEMENT.get(entity).getDirection());
////            }
//        }
        entityArray.sort(positionsComparator);
        for (int i = 0; i < entityArray.size; i++) {
            Entity entity = entityArray.get(i);
            MovementComponent movement = Mappers.MOVEMENT.get(entity);


            movement.setDirection(direction);
            if(Mappers.ANIMATION.get(entity)!=null)
                Mappers.ANIMATION.get(entity).setCurrentAnimation(direction);

//            ////System.out.println("Entities direction is "+movement.getDirection());
            if(movement.getDirection()==Direction.none)
                continue;
            movement.move(direction);
//            //todo clean up code
//            if(Mappers.PLAYER.get(entity)!=null){ //move player
//                Mappers.PLAYER.get(entity).setHasUnhandledInput(true);
//                Mappers.PLAYER.get(entity).setDirection(direction);
//            }
//            else{ //move follower
//
//                Mappers.FOLLOWER.get(entity).setHasUnhandledInput(true);
//                Mappers.FOLLOWER.get(entity).setDirection(direction);
//            }
        }
    }

    public void addFollowers(){
        entities = engine.getEntitiesFor(PLAYABLE_CHARACTERS);
        for(Entity player:entities){
            Vector2 position = Mappers.MOVEMENT.get(player).getTargetPosition();

            for(int i = -1; i<=1;i++){
                for(int j = -1; j<=1; j++){
                    Entity entity1 = positionTracker.getEntityAtPos(EntityType.CHARACTER,position.x+i,position.y+j);
                    if(entity1!=null&&!entities.contains(entity1,true)){
                        entity1.add(engine.createComponent(FollowerTag.class));
                        //System.out.println("Added follower");
                    }
                }
            }

        }

    }
    public void removeFollowers(){
        entities = engine.getEntitiesFor(Family.all(FollowerTag.class).get());
        //remove PlayerTag from those entities

        for(int i = entities.size()-1;i>=0;i--){ //need to remove from back first
            Entity entity = entities.get(i);
            entity.remove(FollowerTag.class);
        }
    }
    public void playDialogue(){
        entities = engine.getEntitiesFor(Family.all(PlayerTag.class, Position.class).get());
        for(Entity entity:entities){
            Vector2 position = Mappers.MOVEMENT.get(entity).getTargetPosition();

            Entity character = null;
            Vector2[] aroundPos = new Vector2[]{
                    new Vector2(position.x-1,position.y),
                    new Vector2(position.x+1,position.y),
                    new Vector2(position.x,position.y+1),
                    new Vector2(position.x,position.y-1),

                    new Vector2(position.x-1,position.y+1),
                    new Vector2(position.x+1,position.y+1),
                    new Vector2(position.x-1,position.y-1),
                    new Vector2(position.x+1,position.y-1),
            };
            for(Vector2 pos: aroundPos){
                if(positionTracker.getEntityAtPos(EntityType.CHARACTER,pos.x,pos.y)!=null){
                    character = positionTracker.getEntityAtPos(EntityType.CHARACTER,pos.x,pos.y);
                    //System.out.println("Detected character in vicinity: "+character);
                    //play that character's dialogue
                    if(character!=null&&Mappers.DIALOGUE.get(character)!=null){
                        //System.out.println("Added speaker component");

                        DialogueComponent dialogueComponent = Mappers.DIALOGUE.get(character);
                        String speakerAbbrev = dialogueComponent.nextLine().split(":")[0];

                        Entity speaker = EntityFactory.getCharacter(characters.getCharacterName(speakerAbbrev));
                        if(speaker!=null){
                            //clears all former speakers
                            ImmutableArray<Entity> formerSpeakers = engine.getEntitiesFor(Family.all(SpeakerTag.class).get());
                            for(Entity formerSpeaker:formerSpeakers){
                                formerSpeaker.remove(SpeakerTag.class);
                            }
                            //adds speech bubble to this speaker
                            speaker.add(engine.createComponent(SpeakerTag.class));
                        }

                        if(Mappers.SPEAKER.get(entity)==null)
                        {
                            character.add(engine.createComponent(SpeechTriggererTag.class));
                        }
                        if(dialogueComponent.isDone()){ //has speaker tag
                            System.out.println("EXECUTED speech triggerer tag removal");
                            character.remove(SpeechTriggererTag.class);//remove it
                            ImmutableArray<Entity> formerSpeakers = engine.getEntitiesFor(Family.all(SpeakerTag.class).get());
                            for(Entity formerSpeaker:formerSpeakers){
                                formerSpeaker.remove(SpeakerTag.class);
                            }
                        }
                    }
                    break;
                }
            }


        }


    }
    public void restart(){
        playScreen.restart();
    }





    public void debugPlayerPosition(){
        ImmutableArray<Entity> entities = engine.getEntitiesFor(PLAYABLE_CHARACTERS);

        for(Entity entity:entities){
            if(Mappers.POSITION.get(entity)!=null)
            System.out.println("Entity "+entity+" is at position "+Mappers.POSITION.get(entity));
            if(Mappers.POSITION_TRACKER.get(entity)!=null){

                System.out.println("Entity "+entity+" has position tracker key "+positionTracker.getKeyForEntity(entity));
            }
        }
    }

}
