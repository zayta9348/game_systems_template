package snow.zhen.zayta.main.engine.input;

import com.badlogic.gdx.input.GestureDetector;

import snow.zhen.zayta.main.engine.PlayController;
import snow.zhen.zayta.main.engine.movement.Direction;


public class SwipeController extends GestureDetector.GestureAdapter {
    private PlayController playController;
    public SwipeController(PlayController playController){
        this.playController = playController;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        //velX and velY are in pixels
        ////log.debug("fling occurred gestureinputhandler");
        if(Math.abs(velocityX)>Math.abs(velocityY)){
            if(velocityX>0){
                playController.movePlayer(Direction.right);
            }else {
                playController.movePlayer(Direction.left);
            }
        }else{
            if(velocityY>0) {
                playController.movePlayer(Direction.down);
            }
            else {
                playController.movePlayer(Direction.up);
            }
        }
        return false;//key! return false, else stage wont work.
    }

}
