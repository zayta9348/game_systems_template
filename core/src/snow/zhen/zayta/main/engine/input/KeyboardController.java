package snow.zhen.zayta.main.engine.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

import snow.zhen.zayta.main.engine.PlayController;
import snow.zhen.zayta.main.engine.movement.Direction;

public class KeyboardController extends InputAdapter {//controls keyboard input controls in the gameplay
    private PlayController playController;
    public KeyboardController(PlayController playController){
        this.playController = playController;
    }
    @Override
    public boolean keyDown(int keycode) {

        if(keycode== Input.Keys.LEFT){

            playController.movePlayer(Direction.left);
        }
        else if(keycode==Input.Keys.RIGHT){

            playController.movePlayer(Direction.right);
        }
        else if(keycode==Input.Keys.UP){
            playController.movePlayer(Direction.up);
        }
        else if(keycode==Input.Keys.DOWN){
            playController.movePlayer(Direction.down);

        }
//        if(keycode==Input.Keys.Z)
//        {
//            engine.undoMove();
//        }
//        if(keycode==Input.Keys.E)
//        {
//            engine.debug();
//        }
        if(keycode==Input.Keys.P){
            playController.debugPlayerPosition();
        }
        if(keycode==Input.Keys.R)
        {
            playController.restart();
        }
        if(keycode==Input.Keys.SPACE){
            playController.addFollowers();
        }

        if(keycode==Input.Keys.X){
            playController.playDialogue();
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        //player stops moving

//        engine.direction = Direction.none;

        if(keycode==Input.Keys.SPACE){
            playController.removeFollowers();
        }
        return true;
    }


//
//    /*Collision detection*/
//    private boolean canPush(Entity entity, Direction direction){
//        if(entity==null)
//            return true;
//        if(entity.is(EntityType.CRATE)){
//            return canCrateMove(getCollidedEntity(entity,direction));
//        }
//        return !entity.is(EntityType.WALL);
//    }
//    private boolean canCrateMove(Entity collidedEntity){
//        return collidedEntity==null||collidedEntity.is(EntityType.GOAL);
//    }
//
//    //returns the Entity that the moveableEntity will collide with, if it moves in the specified direction
//    private Entity getCollidedEntity(Entity moveableEntity, z_oop.sokoban.movement.Direction direction){
//        float x = Math.round(moveableEntity.getX())+direction.directionX,
//                y = Math.round(moveableEntity.getY())+direction.directionY;
//        Entity entityTemplate = positionTracker.getEntityAtPos(x,y);
////        ////System.out.println("CollidedEntity is "+entityTemplate+" and psition of movable entity is "+moveableEntity.getX()+","+moveableEntity.getY());
//        return entityTemplate;
//    }

}
