package snow.zhen.zayta.main.engine.entities.tags;

import com.badlogic.ashley.core.Component;

import snow.zhen.zayta.main.engine.entities.Characters;

public class CharacterTag implements Component {

    private Characters.CharacterName characterName = Characters.CharacterName.ANONYMOUS1;

    public void setCharacterName(Characters.CharacterName characterName) {
        this.characterName = characterName;
    }

    public Characters.CharacterName getCharacterName() {
        return characterName;
    }
}
