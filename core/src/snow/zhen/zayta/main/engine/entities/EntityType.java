package snow.zhen.zayta.main.engine.entities;

public enum EntityType {
    WALL,
    GOAL,
    CHARACTER,
    HOLE,
    NULL
}
