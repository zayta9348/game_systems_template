package snow.zhen.zayta.main.engine.render.animation;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.render.TextureComponent;
import snow.zhen.zayta.main.engine.render.animation.AnimationComponent;
import snow.zhen.zayta.main.engine.movement.Direction;

public class AnimationSystem extends IteratingSystem {
    private float animationStateTime = 0;
    private static Family FAMILY= Family.all(
            AnimationComponent.class,
            MovementComponent.class,
            TextureComponent.class
    ).get();
    public AnimationSystem(int priority) {
        super(FAMILY,priority);
    }


    @Override
    protected void processEntity(Entity entity, float deltaTime) {
            animationStateTime+=deltaTime;
            AnimationComponent animationComponent = Mappers.ANIMATION.get(entity);
            TextureComponent textureComponent = Mappers.TEXTURE.get(entity);
            Direction direction = Mappers.MOVEMENT.get(entity).getDirection();
            TextureRegion textureRegion = animationComponent.getFrame(animationStateTime,direction);
            textureComponent.setRegion(textureRegion);

    }
}
