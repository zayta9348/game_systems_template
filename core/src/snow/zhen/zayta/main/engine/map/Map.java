package snow.zhen.zayta.main.engine.map;

import java.util.Iterator;
import java.util.Set;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.engine.EntityFactory;
import snow.zhen.zayta.main.engine.dialogue.AnonymousIterator;
import snow.zhen.zayta.main.engine.entities.Characters;
import snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks.YanseColor;
import snow.zhen.zayta.main.engine.dialogue.DialogueFactory;

public class Map {

    //temporarily only Tenyu is available
//    private CharacterName characterName = CharacterName.TENYU;
//    private BiMap<CharacterName, Entity> characterNameEntityBiMap;
    private MapParser mapParser;
    private int curLvl =0;
    //textures
    private EntityFactory entityFactory;
    //placing characters
    private Characters characters;
    private Set<String> characterNames;
    private Iterator<String> characterNameIterator;


    private int numGoals;

    //this is specific to the map being parsed
//    private final char FLOOR_ID     = ' ';
//    private final char WALL_ID      = '#';
//    private final char GOAL_ID     = '.';
//    private final char CRATE_ID     = '$';
    private final char PLAYER_ID   = '@';
//    private final char CRATE_ON_GOAL_ID = '*';
//    private final char PLAYER_ON_GOAL_ID = '+';
//    private final char NIGHTER_ID = 'n';


    private int mapWidth,mapHeight;
    
    public Map(EntityFactory entityFactory, Characters characters){
        this.mapParser = new MapParser();
        this.characters = characters;
        this.entityFactory = entityFactory;
//        this.characterNameEntityBiMap = new BiMap<CharacterName, Entity>();

    }

    public void init(int lvl){

        //set new lvl data
        //System.out.println("Map init lvl "+lvl);

        curLvl = lvl;
        MapParser.Level level = mapParser.getLevel(lvl);
        mapWidth = level.getWidth();
        mapHeight = level.getHeight();
        entityFactory.init(mapWidth,mapHeight);
        System.out.println("MapWidth: "+mapWidth);
        System.out.println("MapHeight: "+mapHeight);
        numGoals=0;
        characterNames = DialogueFactory.parseEpisode(curLvl).keySet();
        characterNameIterator = characterNames.iterator();

        char[] mapData = level.getLvlData().toCharArray();

//        //System.out.println("Map:"+Arrays.toString(mapData));
        createEntities(mapData);
    }

    private void createEntities(char [] mapData){
        entityFactory.addBackground(mapWidth,mapHeight,0,0);
        for(int i = 0; i<mapWidth;i++){//player must be added first in order for collision mechs with player to work. odd.
            for(int j = 0; j<mapHeight;j++){
                entityFactory.addFloorTile(GameConfig.ENTITY_SIZE,GameConfig.ENTITY_SIZE,i,j);

            }
        }
        for(int i = 0; i < mapWidth; i++){
            for(int j = 0; j<mapHeight; j++){
                char id = mapData[i+j*mapWidth];
                float x = i;//+(GameConfig.VIRTUAL_WIDTH-mapWidth)/2;
                float y = mapHeight-j;//+(GameConfig.VIRTUAL_HEIGHT-mapHeight)/2-1;
                addEntity(id,x,y);
            }
        }
        
    }
//    private void addPlayer(char id, float x, float y){
//        if(id==PLAYER_ID){
//            entityFactory.addPlayer(Characters.CharacterName.TENYU,x,y);
//        }
//    }
    //Uppercase letters add blocks, lowercase add goals, others add nighters.
    private void addEntity(char id, float x, float y){
//        if(id==PLAYER_ID)
//            return;
        if(id==PLAYER_ID){
            entityFactory.addPlayer(Characters.CharacterName.TENYU,x,y);
        }
        else if(Character.isUpperCase(id)){ //todo modify this & yansecolors too use digit for color of wall
            entityFactory.addWall(x,y, YanseColor.get(id));
        }
        else if(Character.isLetter(id)){
            entityFactory.addGoal(x,y, YanseColor.get(id));
            numGoals++;
        }
        else if(!Character.isWhitespace(id)){
            System.out.println("Added nighter at id "+id);
                if(characterNameIterator.hasNext()) {
                    String nameAbbrev = characterNameIterator.next();
                    Characters.CharacterName charName = characters.getCharacterName(nameAbbrev);
                    entityFactory.addNighter(charName,
                            x, y, curLvl);

                    System.out.println("Added nighter from iterator at id "+id+" and name "+charName+" and abbrev "+nameAbbrev);
                }
                else{
                    entityFactory.addNighter(AnonymousIterator.getRandomAnon(),x,y,curLvl);
                }
//            entityFactory.addNighter(Characters.getCharacterName(id),x,y,curLvl);

        }
//        switch (id){
////            case FLOOR_ID:
////                break;
////            case WALL_ID:
////                entityFactory.addWall(x, y, YanseColors.ORANGE);
////                break;
////            case GOAL_ID:
////                entityFactory.addGoal(x, y);
////                numGoals++;
////                break;
////            case CRATE_ID:
////                entityFactory.addWall(x,y,YanseColors.ORANGE);
////                break;
////            case PLAYER_ID:
////                entityFactory.addPlayer(Characters.CharacterName.TENYU,x,y);
////                break;
////            case CRATE_ON_GOAL_ID:
////                entityFactory.addGoal(x,y);
////                entityFactory.addWall(x,y,YanseColors.ORANGE);
////                numGoals++;
////                break;
////            case PLAYER_ON_GOAL_ID:
////                entityFactory.addPlayer(Characters.CharacterName.TENYU,x,y);
////                entityFactory.addGoal(x,y);
////                break;
////            case NIGHTER_ID:
////                entityFactory.addNighter(Characters.CharacterName.LORALE,x,y, curLvl);
////                break;
////        }
    }
    public int getTotalLvls(){
        return 3;
    }
    public int getMapWidth() {
        return mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }
    public int getNumGoals(){
        return numGoals;
    }

}
