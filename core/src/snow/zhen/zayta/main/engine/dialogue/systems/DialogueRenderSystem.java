package snow.zhen.zayta.main.engine.dialogue.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent;
import snow.zhen.zayta.main.engine.dialogue.components.SpeakerTag;
import snow.zhen.zayta.main.engine.dialogue.components.SpeechTriggererTag;
import snow.zhen.zayta.main.engine.render.TextureComponent;

//system for rendering the dialogue
public class DialogueRenderSystem extends EntitySystem {
    private static final Family triggerers = Family.all(DialogueComponent.class, SpeechTriggererTag.class).get();

    private static final Family speakerFamily = Family.all(SpeakerTag.class, TextureComponent.class/*CharacterTag.class*/).get();
    private final Viewport viewport;
    private final SpriteBatch batch;
    private final BitmapFont font;

//    private final GlyphLayout layout = new GlyphLayout();
    private TextureRegion txtBckgrnd; //private Skin skin;
    private float leftOffset = 0.1f, textOffset = GameConfig.ENTITY_SIZE+0.5f;
    private float dialogueBoxWidth,dialogueBoxHeight, dialoguePosY;

    public DialogueRenderSystem(int priority, Viewport viewport, SpriteBatch batch, TextureRegion txtBckgrnd) {
        super(priority);
        this.viewport = viewport;
        this.batch = batch;
        this.txtBckgrnd = txtBckgrnd;
//        this.skin = skin;
        //customize font
        this.font = new BitmapFont();
        float scaleX = GameConfig.VIRTUAL_WIDTH / GameConfig.WIDTH;
        float scaleY = GameConfig.VIRTUAL_HEIGHT / GameConfig.HEIGHT;
        float fontScale = 2;
        float scale = Math.max(scaleX,scaleY);
        font.setUseIntegerPositions(false);
        font.setColor(Color.BLACK);
        font.getData().setScale(fontScale * scale);


    }
    @Override
    public void update(float deltaTime) {
        ImmutableArray<Entity> dialogues = getEngine().getEntitiesFor(triggerers);

        ImmutableArray<Entity> speakers = getEngine().getEntitiesFor(speakerFamily);

        dialogueBoxWidth =viewport.getWorldWidth()-2*leftOffset;
        dialogueBoxHeight = viewport.getWorldHeight()/4;
        dialoguePosY = 0;//viewport.getWorldHeight()-dialogueBoxHeight;

//        float viewportOffsetX =viewport.getCamera().position.x-viewport.getWorldWidth()/2,
//                viewportOffsetY = viewport.getCamera().position.y-viewport.getWorldHeight()/2;

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
//        System.out.println("Dialogue entitiies is "+entities.size());
        for(int i = 0; i<dialogues.size(); i++){
            batch.draw(txtBckgrnd,leftOffset,dialoguePosY,dialogueBoxWidth,dialogueBoxHeight);

            Entity entity = dialogues.get(i);
            drawDialogue(entity,i);
        }
        for(int i = 0; i<speakers.size(); i++){

            Entity entity = speakers.get(i);
            drawSpeaker(entity,i);
        }
        batch.end();
    }

    private void drawDialogue(Entity entity,int i){
        String dialogue = Mappers.DIALOGUE.get(entity).currentLine();

        font.draw(batch,dialogue,leftOffset+textOffset, dialoguePosY+dialogueBoxHeight-((i+1)*font.getLineHeight()/2),
                dialogueBoxWidth-leftOffset-textOffset,Align.left,true);
////        layout.setText(font,dialogue,0,dialogue.length(),Color.BLACK,dialogueBoxWidth, Align.left,true,"...");
//        font.draw(batch,dialogue,viewportOffsetX+leftOffset+textOffset, viewportOffsetY+dialoguePosY+dialogueBoxHeight-((i+1)*font.getLineHeight()/2),
//                dialogueBoxWidth-leftOffset-textOffset,Align.left,true);
//        font.draw(batch,dialogue,
//                leftOffset+textOffset, dialoguePosY+dialogueBoxHeight-((i+1)*layout.height)); //.5 is offset from the top, 1 is offset from left

    }

    private void drawSpeaker(Entity entity, int i){
        //displays their names
//        font.draw(batch,Mappers.CHARACTER.get(entity).getCharacterName().toString(),
//                leftOffset+0.2f, dialoguePosY+dialogueBoxHeight); //.5 is offset from the top, 1 is offset from left

//        //uncomment if u want to draw their face too next to their speach

        batch.draw(Mappers.TEXTURE.get(entity).getFirstRegion(),
                leftOffset+0.2f,
                dialoguePosY+dialogueBoxHeight/1.8f,1,1);
//        batch.draw(Mappers.TEXTURE.get(entity).getFirstRegion(),
//                viewportOffsetX+leftOffset+0.2f,
//                viewportOffsetY+dialoguePosY+dialogueBoxHeight/1.8f,1,1);
    }

}
