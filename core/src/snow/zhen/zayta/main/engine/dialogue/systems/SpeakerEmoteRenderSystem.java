package snow.zhen.zayta.main.engine.dialogue.systems;


import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import snow.zhen.zayta.main.GameConfig;
import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.dialogue.components.SpeakerTag;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;

public class SpeakerEmoteRenderSystem extends IteratingSystem {
    private final Viewport viewport;
    private final SpriteBatch batch;
//    private final BitmapFont font;

    private final TextureRegion textureRegion;

    private final GlyphLayout layout = new GlyphLayout();

    private Array<Entity> renderQueue = new Array<Entity>();
    private static final Family SPEAKERS = Family.all(SpeakerTag.class,Position.class).get();

    private final float EMOTE_WIDTH = 0.5f,EMOTE_HEIGHT=0.5f;


    public SpeakerEmoteRenderSystem(int priority, Viewport viewport, SpriteBatch batch, TextureRegion textureRegion) {
        super(SPEAKERS,priority);
        this.viewport = viewport;
        this.batch = batch;

        this.textureRegion = textureRegion;


    }

    @Override
    public void update(float deltaTime) {

        ImmutableArray<Entity> entities = getEngine().getEntitiesFor(SPEAKERS);
//        System.out.println("Emote size"+entities.size());
        renderQueue.addAll(entities.toArray());

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();

        super.update(deltaTime);

        batch.end();

        renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Position position = Mappers.POSITION.get(entity);
//        if(Mappers.COLOR.get(entity)!=null){
//            font.setColor(Mappers.COLOR.get(entity).getColor());
//        }


//        layout.setText(font,/*"Position: ("+position.getX()+","+position.getY()+")\n"+
//                "Position Raw Key: "+PositionTracker.generateKey(position.getX(),position.getY())+"\n"+
//                "Bounds Raw Key: "+PositionTracker.generateKey(bounds.getX(),bounds.getY())+"\n"+*/
//                /*"\\../"*/"o/\\o");
//        font.draw(batch, layout, position.getX()+GameConfig.ENTITY_SIZE/2+0.25f, position.getY() + GameConfig.ENTITY_SIZE + layout.height+0.1f);//0.1f is offset from bottom
        batch.draw(textureRegion,position.getX()+EMOTE_WIDTH/2,position.getY()+GameConfig.ENTITY_SIZE+0.1f,EMOTE_WIDTH,EMOTE_HEIGHT);
//        batch.draw(textureRegion,1,1,1,1);


    }


}