package snow.zhen.zayta.main.engine.dialogue;

import java.util.Random;

import snow.zhen.zayta.main.engine.entities.Characters;

public class AnonymousIterator {
    public static Characters.CharacterName [] anonymousCharacters = new Characters.CharacterName[]{
            Characters.CharacterName.ANONYMOUS1,Characters.CharacterName.ANONYMOUS2,
            Characters.CharacterName.ANONYMOUS3,Characters.CharacterName.ANONYMOUS4,
            Characters.CharacterName.ANONYMOUS5
    };
    public static Characters.CharacterName getRandomAnon(){
        int r = new Random().nextInt(anonymousCharacters.length);
        return anonymousCharacters[r];
    }
}
