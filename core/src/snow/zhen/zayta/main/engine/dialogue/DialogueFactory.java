package snow.zhen.zayta.main.engine.dialogue;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;


import java.util.ArrayList;
import java.util.Arrays;

import snow.zhen.zayta.main.assets.Files;
import snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent;
import snow.zhen.zayta.main.engine.entities.Characters;
import snow.zhen.zayta.util.BiMap;

public class DialogueFactory {
//    private static final DialogueFileParser dialogueFileParser = new DialogueFileParser();;
//    private int curLvl; //depending on what curLvl is, there will be dif dialogue
    private PooledEngine engine;

    //loads episodes
    private static String [] episodes = Files.story.readString().split("episode [0-9]:");
    ;
    private final ArrayList<String> defaultDialogue;
    private final ArrayList<String> anonDialogue;
    private Characters characters;
    public DialogueFactory(PooledEngine engine, Characters characters) {
        this.characters = characters;

//        loadFile(Files.story);
        this.engine = engine;
        defaultDialogue = new ArrayList<String>();
        defaultDialogue.add("tyu: ...");
        defaultDialogue.add("tyu: (I have nothing to ask)");
//        defaultDialogue = new String[]{"...", "I have nothing to ask."};
//        System.out.println(Arrays.toString(episodes));
        anonDialogue = new ArrayList<String>();

    }

    //creates the dialogue component for @param entity with the charactername and curLvl
    public void addDialogue(Entity entity, Characters.CharacterName characterName, int curLvl){
        BiMap<String, ArrayList<String>> biMap =parseEpisode(curLvl);

        snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent dialogueComponent = engine.createComponent(DialogueComponent.class);
        String trigger= characters.getCharacterAbbrev(characterName).trim().toLowerCase();
//        if(trigger.matches("an.*")){//anonymous case
////            dialogueComponent.setDialogue();
//            System.out.println("Dialogue biMap is now "+biMap.toString());
//        }
//        else
            if(biMap.containsKey(trigger)){
//            DialogueComponent dialogueComponent = engine.createComponent(DialogueComponent.class);

            dialogueComponent.setDialogue(biMap.get(trigger));
            //System.out.println("Added dialgoue component to entity");
        }
        else{
            dialogueComponent.setDialogue(defaultDialogue);
        }

        entity.add(dialogueComponent);

    }



    //splits file into episodes
//    private void loadFile(FileHandle storyFile){
//        String fullStory = storyFile.readString();
////        episodes = fullStory.replaceAll("label","]").split("]");
//        episodes = fullStory.split("episode [0-9]:");
//
//
////        for(int i = 0; i<episodes.length;i++){
////            //System.out.println("Episode "+i+":"+episodes[i]+"\n\n\n");
////        }
////        //System.out.println("numEpisodes:"+episodes.length);
//    }

//    public String getEpisode(int ep){
//        parseEpisode(ep);
//        return episodes[ep%episodes.length];
//    }


    public static BiMap<String,ArrayList<String>> parseEpisode(int ep){
        String episode = episodes[ep%episodes.length];
        BiMap<String, ArrayList<String>> dialogue = new BiMap<String,ArrayList<String>>();
        //key: character that starts the scene. value: the scene, which has queue like this: ll: yadiya, ta: yaya
        //todo instead of string as value, make it a pair of parallel arrays? or a bimap where val = dialogue, key = speaker
//        //System.out.println("Episode is "+episode);
        String[] lines = episode.split("\\r?\\n");


        ArrayList<String> dialogueQueue =new ArrayList<String>();

        for(String line:lines){
            if(line.matches("(.*)=(.*)")){
                dialogueQueue = new ArrayList<String>();
                String triggerer = line.split("=")[1].trim().toLowerCase();
                if(!dialogue.containsKey(triggerer))
                    dialogue.put(triggerer,dialogueQueue);
                else
                    dialogueQueue=dialogue.get(triggerer);
            }
            else {
                dialogueQueue.add(line);
//                dialogueQueue.add(line);//line consists of lt: yadiya
            }
        }



//        for(String tr: dialogue.keySet()){
//            System.out.println("Tr= "+tr+" : "+ Arrays.toString(dialogue.get(tr).toArray()));
//        }



        return dialogue;
    }



}
