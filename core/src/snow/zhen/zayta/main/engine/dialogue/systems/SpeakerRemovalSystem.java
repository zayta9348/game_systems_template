package snow.zhen.zayta.main.engine.dialogue.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;

import snow.zhen.zayta.main.common.Mappers;
import snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent;
import snow.zhen.zayta.main.engine.dialogue.components.SpeakerTag;
import snow.zhen.zayta.main.engine.dialogue.components.SpeechTriggererTag;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
//import z_oop.sokoban.entity.entities.Player;

public class SpeakerRemovalSystem extends EntitySystem {

    private static final Family PLAYABLE_CHARACTERS = Family.all(PlayerTag.class, MovementComponent.class).get();
    private static final Family DIALOGUE_STARTERS = Family.all(SpeechTriggererTag.class, DialogueComponent.class, MovementComponent.class).get();
    private static final Family SPEAKERS = Family.all(SpeakerTag.class).get();

    public SpeakerRemovalSystem(int priority){
        super(priority);
    }
    @Override
    public void update(float deltaTime) {
        ImmutableArray<Entity> dialogue_starters = getEngine().getEntitiesFor(DIALOGUE_STARTERS);
        ImmutableArray<Entity> players = getEngine().getEntitiesFor(PLAYABLE_CHARACTERS);

        for(Entity dialogue_starter: dialogue_starters){
            boolean shouldRemove = true;
            MovementComponent dialogue_starterMovement = Mappers.MOVEMENT.get(dialogue_starter);
            Vector2 fpos = dialogue_starterMovement.getTargetPosition();
            for(Entity player:players){
                Vector2 pos = Mappers.MOVEMENT.get(player).getTargetPosition();

                if(isAround(pos,fpos)){
                    shouldRemove=false;
                    break;
                }
            }

            if(shouldRemove){
                removeSpeakers();
                dialogue_starter.remove(SpeechTriggererTag.class);
                Mappers.DIALOGUE.get(dialogue_starter).resetCurIndex();
            }
        }

    }

    //returns whether v2 is around v1
    private boolean isAround(Vector2 v1, Vector2 v2){
        Vector2[] aroundV1 = new Vector2[]{
                new Vector2(v1.x-1,v1.y),
                new Vector2(v1.x+1,v1.y),
                new Vector2(v1.x,v1.y+1),
                new Vector2(v1.x,v1.y-1)//,

//                new Vector2(v1.x-1,v1.y+1),
//                new Vector2(v1.x+1,v1.y+1),
//                new Vector2(v1.x-1,v1.y-1),
//                new Vector2(v1.x+1,v1.y-1),
        };
        float e = .1f;
//        boolean ret = false;
        for(Vector2 v: aroundV1){
            if(v2.epsilonEquals(v,e))
                return true;
        }
        return false;
//        return v2.epsilonEquals(v1Left,e)||v2.epsilonEquals(v1Right,e)||v2.epsilonEquals(v1Up,e)||v2.epsilonEquals(v1Down,e);
    }
    
    private void removeSpeakers(){
        ImmutableArray<Entity> entities = getEngine().getEntitiesFor(SPEAKERS);
        for(Entity speaker: entities){
            speaker.remove(SpeakerTag.class);
        }
    }
}

