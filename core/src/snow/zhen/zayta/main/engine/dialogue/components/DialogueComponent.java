package snow.zhen.zayta.main.engine.dialogue.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Queue;

import java.util.ArrayList;

import snow.zhen.zayta.main.engine.EntityFactory;

public class DialogueComponent implements Component, Pool.Poolable {
    private ArrayList<String> dialogue;
    private int curIndex = 0;
    private boolean isDone = false;
    private boolean hasBeenDone;
    private String curLine = "";
    private String speaker = "";
//    public ArrayList<String> getDialogue() {
//        return dialogue;
//    }
    public String nextLine(){
        String line;
        int i = curIndex%dialogue.size();
//        System.out.println("i is "+i);
        if(i!=0||curIndex==0) {
            line = dialogue.get(i);
            curLine = line;
            if(line.contains(":")){
                speaker = line.split(":")[0];
                curLine = line.split(":")[1];
            }
        }
        else {
            isDone = true;
            hasBeenDone = true;
            line = dialogue.get(dialogue.size()-1);
            if(line.contains(":")){
                speaker = line.split(":")[0];
                curLine = line.split(":")[1];
            }
            else curLine = line;
        }
        curIndex++;
        return line;
    }
    public String currentLine(){
        return curLine;
    }

    public void setDialogue(ArrayList<String> dialogue) {
        this.dialogue = dialogue;
    }

    public boolean isDone() {
        if(isDone) {
            boolean ret = isDone;
            isDone = false;
            curIndex = dialogue.size()-1;//set it to last statement that was said.
            return ret;
        }
        else
            return isDone;
    }
    public void resetCurIndex(){
        if(hasBeenDone)
            curIndex = dialogue.size()-1;
        else curIndex = 0;
    }

    @Override
    public void reset() {
        dialogue = null;
        isDone = false;
        curLine = "";speaker="";
        curIndex = 0;
        hasBeenDone = false;
    }

}
