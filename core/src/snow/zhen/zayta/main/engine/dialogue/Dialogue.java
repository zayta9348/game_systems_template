package snow.zhen.zayta.main.engine.dialogue;

import snow.zhen.zayta.main.engine.entities.Characters;

public class Dialogue {

    private final String triggerer;

    public Dialogue(String triggerer){
        this.triggerer = triggerer;
    }

    public class DialogueLine{
        private final Characters.CharacterName speaker;
        private final String text;

        public DialogueLine(Characters.CharacterName speaker, String text){
            this.speaker = speaker;
            this.text = text;
        }
    }
}
