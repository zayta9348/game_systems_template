package snow.zhen.zayta.main.common;

import com.badlogic.ashley.core.ComponentMapper;

import snow.zhen.zayta.main.engine.entities.tags.CharacterTag;
import snow.zhen.zayta.main.engine.entities.tags.PlayerTag;
import snow.zhen.zayta.main.engine.game_systems.blocks.color_blocks.ColorComponent;
import snow.zhen.zayta.main.engine.dialogue.components.DialogueComponent;
import snow.zhen.zayta.main.engine.dialogue.components.SpeechTriggererTag;
import snow.zhen.zayta.main.engine.game_systems.hand_hold.FollowerTag;
import snow.zhen.zayta.main.engine.game_systems.sokoban_doors.DoorComponent;
import snow.zhen.zayta.main.engine.movement.character_behavior.FaceDirOnNoMovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.MovementComponent;
import snow.zhen.zayta.main.engine.movement.movement_components.Position;
import snow.zhen.zayta.main.engine.movement.position_tracker.PositionTrackerComponent;
import snow.zhen.zayta.main.engine.movement.world_wrap.WorldWrapComponent;
import snow.zhen.zayta.main.engine.render.animation.AnimationComponent;
import snow.zhen.zayta.main.engine.render.TextureComponent;
import snow.zhen.zayta.main.engine.game_systems.holes.HolePocketComponent;


public class Mappers {
    public static final ComponentMapper<PlayerTag> PLAYER = ComponentMapper.getFor(PlayerTag.class);


    public static final ComponentMapper<TextureComponent> TEXTURE = ComponentMapper.getFor(TextureComponent.class);
    public static final ComponentMapper<AnimationComponent> ANIMATION = ComponentMapper.getFor(AnimationComponent.class);
    public static final ComponentMapper<MovementComponent> MOVEMENT = ComponentMapper.getFor(MovementComponent.class);
    public static final ComponentMapper<Position> POSITION = ComponentMapper.getFor(Position.class);
    public static final ComponentMapper<PositionTrackerComponent> POSITION_TRACKER = ComponentMapper.getFor(PositionTrackerComponent.class);

    public static final ComponentMapper<WorldWrapComponent> WORLD_WRAP = ComponentMapper.getFor(WorldWrapComponent.class);

    public static final ComponentMapper<FaceDirOnNoMovementComponent> DEFAULT_DIRECTION = ComponentMapper.getFor(FaceDirOnNoMovementComponent.class);


    public static final ComponentMapper<HolePocketComponent> HOLE_POCKET = ComponentMapper.getFor(HolePocketComponent.class);
    public static final ComponentMapper<ColorComponent> COLOR = ComponentMapper.getFor(ColorComponent.class);

    public static final ComponentMapper<FollowerTag> FOLLOWER = ComponentMapper.getFor(FollowerTag.class);

    public static final ComponentMapper<CharacterTag> CHARACTER = ComponentMapper.getFor(CharacterTag.class);
    public static final ComponentMapper<DialogueComponent> DIALOGUE = ComponentMapper.getFor(DialogueComponent.class);
    public static final ComponentMapper<SpeechTriggererTag> SPEAKER = ComponentMapper.getFor(SpeechTriggererTag.class);

    public static final ComponentMapper<DoorComponent> DOOR = ComponentMapper.getFor(DoorComponent.class);
    private Mappers(){}
}
