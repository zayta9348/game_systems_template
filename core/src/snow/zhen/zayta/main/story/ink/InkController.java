package snow.zhen.zayta.main.story.ink;

import com.badlogic.gdx.files.FileHandle;
import com.bladecoder.ink.runtime.Choice;
import com.bladecoder.ink.runtime.Story;

import snow.zhen.zayta.main.assets.Files;

public class InkController {
    FileHandle fileHandle = Files.inkStory;
    String jsonString = fileHandle.readString().replace('\uFEFF', ' ');;
    Story story;
    public InkController() throws Exception {
        story = new Story(jsonString);
    }
    public void nextLine(){
        try {
            if(story.canContinue()){
                String line = story.Continue();
                System.out.println(line);
            }
            else if (story.getCurrentChoices().size() > 0) {
                for (Choice c : story.getCurrentChoices()) {
                    System.out.println("Choice: "+c.getText());
                }

                story.chooseChoiceIndex(0);
            }
        }
        catch (Exception e){

        }

    }



}
