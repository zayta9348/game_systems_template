package snow.zhen.zayta.main.story.ink;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.bladecoder.ink.runtime.Choice;
import com.bladecoder.ink.runtime.Story;

import snow.zhen.zayta.main.assets.Files;

public class InkComponent implements Component {
    FileHandle fileHandle = Files.inkStory;
    String jsonString = fileHandle.readString().replace('\uFEFF', ' ');;

    Story story;
    private String stitch;
    public InkComponent(String triggerer) {
        try {

            story = new Story(jsonString);
            this.stitch = triggerer;
        }
        catch (Exception e){

        }
    }

    public void nextLine(){

        try {

            story.choosePathString(stitch);
            if(story.canContinue()){
                String line = story.Continue();
                System.out.println(line);
            }
            else if (story.getCurrentChoices().size() > 0) {
                for (Choice c : story.getCurrentChoices()) {
                    System.out.println("Choice: "+c.getText());
                }

                story.chooseChoiceIndex(0);
            }
        }
        catch (Exception e){

        }


    }
}
