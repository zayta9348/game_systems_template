package snow.zhen.zayta.main.assets;

public class RegionNames {


    //Sokoban
    public static final String CRATE_SILVER_PURPLE = "crates/silver_purple/crate_silver_purple";
    public static final String CRATE_LBLUE = "crates/lt_blue/crate_ltblue";
    public static final String CRATE_BLUE = "crates/blue/crate_blue";
    public static final String CRATE_YELLOW_BLACK = "crates/yellow_black/crate_yellow";

    public static final String BRICK = "brick/brick";
    public static final String BRICK_STONE_CRATE = "brick/brick_stone";
    public static final String BRICK_YELLOW = "brick/brick_yellow";
    public static final String BRICK_LAVENDER = "brick/brick_lavender";

    public static final String[] OVERLAY = {"overlay/one","overlay/two","overlay/three","overlay/four"};

    //backgrounds

    public static final String FUTURE_INDUSTRY = "background/future_industrial/Futuristic Industrial Tileset";
    public static final String BLUE_BIT_MATRIX = "background/blue-bit-1";
    public static final String YELLOW = "background/yellow";
    public static final String VR_ROOM_P1 = "floor/vr_pattern1/vr_pattern1";
    public static final String LAND_SNOW = "floor/land/snow";
    public static final String FLOOR_PLAIN = "floor/land/plain";
    public static final String HOLE = "floor/hole";
    public static final String GROUND_GRAY = "floor/ground/ground";
    //==UI==//
    public static final String UI_TOUCHPAD_BCKGRND = "ui-btns/touchpad-bckgrnd";
    //==UI btns==//
    public static final String BTN_CART = "ui-btns/btn-cart";
    public static final String BTN_CHECK = "ui-btns/btn-check";
    public static final String BTN_CONSOLE = "ui-btns/btn-console";
    public static final String BTN_DELETE = "ui-btns/btn-delete";
    public static final String BTN_DOWN = "ui-btns/btn-down";
    public static final String BTN_DOWNLOAD = "ui-btns/btn-download";
    public static final String BTN_GRID = "ui-btns/btn-grid";
    public static final String BTN_HOME = "ui-btns/btn-home";
    public static final String BTN_INFO = "ui-btns/btn-info";
    public static final String BTN_LEFT = "ui-btns/btn-left";
    public static final String BTN_LIST = "ui-btns/btn-list";
    public static final String BTN_MINUS = "ui-btns/btn-minus";
    public static final String BTN_MULTIPLAYER = "ui-btns/btn-multiplayer";
    public static final String BTN_MUSIC = "ui-btns/btn-music";
    public static final String BTN_PAUSE = "ui-btns/btn-pause";
    public static final String BTN_PLAY = "ui-btns/btn-play";
    public static final String BTN_PLUS = "ui-btns/btn-plus";
    public static final String BTN_QUESTION = "ui-btns/btn-question";
    public static final String BTN_RANK = "ui-btns/btn-rank";
    public static final String BTN_REFRESH = "ui-btns/btn-refresh";
    public static final String BTN_RIGHT = "ui-btns/btn-right";
    public static final String BTN_SETTINGS = "ui-btns/btn-settings";
    public static final String BTN_SHARE = "ui-btns/btn-share";
    public static final String BTN_STOP = "ui-btns/btn-stop";
    public static final String BTN_TROPHY = "ui-btns/btn-trophy";
    public static final String BTN_UNDO = "ui-btns/btn-undo";
    public static final String BTN_UP = "ui-btns/btn-up";
    public static final String BTN_X = "ui-btns/btn-x";

    public static final String DIALOGUE_BOX = "dialogue/dialogue-box";

    public static final String EMOTE_DOTS3 = "emotes/emote_dots3";
//
//    //maps
//    public static final String MAPS_MEMLAB1 = "gameplay/experiment/maps/memLab/memLab1.tmx";
//    public static final String MAPS_MEMLAB2 = "gameplay/experiment/maps/memLab/memLab2.tmx";
//    public static final String MAPS_MEMLABBIG = "gameplay/experiment/maps/memLab/memLabBig.tmx";
//
    //scenes
    public static final String INTRO = "scenes/intro/intro";
    //characters
    public static final String LORALE = "characters/lorale/lorale";

    public static final String TENYU ="characters/tenyu/tenyu";
    public static final String JOLTAN = "characters/joltan/joltan";


    public static final String LETRA ="characters/letra/letra";

    public static final String TARIA ="characters/taria/taria";
    public static final String XIF ="characters/xif/xif";


    public static final String ANONYMOUS = "characters/anon/anon";



}
