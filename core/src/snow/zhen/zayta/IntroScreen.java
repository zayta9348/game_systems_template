package snow.zhen.zayta;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import snow.zhen.zayta.main.Game;
import snow.zhen.zayta.main.assets.RegionNames;
import snow.zhen.zayta.main.ScreenBase;
import snow.zhen.zayta.main.UserData;
import snow.zhen.zayta.main.assets.AssetDescriptors;

public class IntroScreen extends ScreenBase {

    //resorces
    private UserData userData;
    private Array<TextureAtlas.AtlasRegion> atlasRegions;
    TextureAtlas textureAtlas;
    //scene2d
    private Table table;
//    private ImageButton sceneBtn; private ImageButton.ImageButtonStyle scene;

    //scene management
    private int frame;
    public IntroScreen(Game game) {
        super(game);
        this.userData = game.getUserData();

        textureAtlas = assetManager.get(AssetDescriptors.GAMEPLAY);
        atlasRegions = textureAtlas.findRegions(RegionNames.INTRO);
        frame = 0;
    }

    @Override
    protected Actor createUi() {

        table = new Table();
        frame = 0;
        next();
        table.setFillParent(true);
        table.clearChildren();
        table.setDebug(true);


        Skin uiskin = assetManager.get(AssetDescriptors.UI_SKIN);
//        table.add(sceneBtn(uiskin)).expand().fill();
        table.setTouchable(Touchable.enabled);
        table.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                next();
            }
        });

//        next();
        return table;
    }
//    private Button sceneBtn(Skin skin){
//        scene = new ImageButton.ImageButtonStyle();
//        sceneBtn = new ImageButton(scene);
//        sceneBtn.setVisible(false);
//        sceneBtn.addListener(new ChangeListener() {
//            @Override
//            public void changed(ChangeEvent event, Actor actor) {
//                next();
//            }
//        });
//        return sceneBtn;
//    }

    private void next(){

        ////System.out.println("Frame: "+frame+" out of "+atlasRegions.size);
        if(frame%atlasRegions.size==0&&frame!=0){
            frame = 0;
            game.setPlayScreen(game.getUserData().getNumCompleted());

        }
        else {
            TextureRegion img = atlasRegions.get(frame % atlasRegions.size);
//            scene.imageUp = new TextureRegionDrawable(img);

            table.setBackground(new TextureRegionDrawable(img));

            frame++;
        }

    }
}
