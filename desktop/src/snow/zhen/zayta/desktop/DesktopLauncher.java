package snow.zhen.zayta.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import snow.zhen.zayta.main.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "NUR_Nighters";
		config.height = 300;
		config.width = 300;
		new LwjglApplication(new Game(), config);
	}
}
